# Openhab 1.6.2
# * configuration is injected
#
FROM armv7/armhf-ubuntu
MAINTAINER Craig Hamilton <craigh@quailholdings.com>


RUN apt-get update && apt-get -y install influxdb-client

ENTRYPOINT ["influx"]

# doesnt work, need to pass in container's host's IP
#CMD ["-host="localhost'"]
